from django.http import JsonResponse

from common.json import ModelEncoder

from .models import Attendee

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


def api_list_attendees(request, conference_id):
    attendees = Attendee.objects.filter(conference=conference_id)
    return JsonResponse(attendees, encoder=AttendeeListEncoder, safe=False)

    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    # attendees = [{"name": attendee.name, "href": attendee.get_api_url(),} for attendee in Attendee.objects.filter(conference=conference_id)]
    # return JsonResponse({"attendees": attendees})


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created",]

    def get_extra_data(self, o):
        return {"conference": {"name": o.conference.name, "href": o.conference.get_api_url()}}
def api_show_attendee(request, pk):
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # attendee = Attendee.objects.get(id=pk)
    # return JsonResponse({"email": attendee.email, "name": attendee.name, "company_name": attendee.company_name, "created": attendee.created, "conference": {"name": attendee.conference.name, "href": attendee.conference.get_api_url()}})
